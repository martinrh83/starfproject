class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name
      t.string :last_name
      t.string :dni
      t.string :legajo
      t.string :address
      t.string :phone

      t.timestamps
    end
  end
end
