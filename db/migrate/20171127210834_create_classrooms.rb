class CreateClassrooms < ActiveRecord::Migration[5.1]
  def change
    create_table :classrooms do |t|
      t.string :nro
      t.string :description

      t.timestamps
    end
  end
end
