class AddFieldsToSubjects < ActiveRecord::Migration[5.1]
  def change
    add_column :subjects, :code, :string
    add_column :subjects, :period, :string
  end
end
