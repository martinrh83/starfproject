class AddRoomToSchedule < ActiveRecord::Migration[5.1]
  def change
    add_reference :schedules, :classroom, foreign_key: true
  end
end
