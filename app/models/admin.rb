class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  rails_admin do
    list do
      field :email
    end
    create do
      field :email
      field :password
      field :password_confirmation
    end
    edit do
      field :email
      field :password
      field :password_confirmation
    end
    show do
      field :email
    end
  end

end
