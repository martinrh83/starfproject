class SchoolYear < ApplicationRecord
  has_many :grades

  rails_admin do
    list do
      field :year
    end
    create do
      field :year
    end
    edit do
      field :year
    end
    show do
      field :year
    end
  end
end
