class Course < ApplicationRecord
  belongs_to :grade
  has_many :schedules

  rails_admin do
    list do
      field :name
      field :grade
    end
    show do
      field :name
      field :grade
    end
    create do
      field :name
      field :grade
    end
    edit do
      field :name
      field :grade
    end
  end
end
