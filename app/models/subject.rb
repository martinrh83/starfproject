class Subject < ApplicationRecord
  belongs_to :grade
  has_many :schedules

  rails_admin do
    list do
      field :code
      field :name
      field :grade
      field :period
    end
    show do
      field :code
      field :name
      field :grade
      field :period
    end
    create do
      field :code
      field :name
      field :grade
      field :period, :enum do
        enum do
          ["Anual","1 cuatrimestre","2 cuatrimestre"]
        end
      end
    end
    edit do
      field :code
      field :name
      field :grade
      field :period, :enum do
        enum do
          ["Anual","1 cuatrimestre","2 cuatrimestre"]
        end
      end
    end
  end

end
