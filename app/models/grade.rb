class Grade < ApplicationRecord
  belongs_to :school_year
  has_many :subjects
  has_many :courses

  rails_admin do
    list do
      field :level
      field :school_year
    end
    create do
      field :level
      field :school_year
    end
    edit do
      field :level
      field :school_year
    end
    show do
      field :level
      field :school_year
    end
  end
end
