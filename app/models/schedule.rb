class Schedule < ApplicationRecord
  belongs_to :course
  belongs_to :subject
  belongs_to :classroom
  
  rails_admin do
    create do
      field :day, :enum do
        enum do
          ["Lunes","Martes","Miercoles","Jueves","Viernes"]
        end
      end
      field :hour_init
      field :hour_end
      field :course
      field :subject
      field :classroom
    end
    edit do
      field :day, :enum do
        enum do
          ["Lunes","Martes","Miercoles","Jueves","Viernes"]
        end
      end
      field :hour_init
      field :hour_end
      field :course
      field :subject
      field :classroom
    end
  end

end
